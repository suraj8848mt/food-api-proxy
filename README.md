# food-api-proxy

NGINX Proxy app for food apis

## Getting started
To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on. Defaults to 8000.
* `API_HOST` - Hostname of the app to forward request to Defaults to `app`.
* `APP_PORT` - Port of the app to forward request to. Defaults to `9000`.